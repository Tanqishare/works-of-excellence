#       Intel IDEA 主流插件
####     Maven Helper 5
        maven依赖管理
---
####     Lombok      5
####     Translation 5
####     JRebel and XRebel for IntelliJ 5
####     JRebel mybatisPlus extension  4
####     Extra Icons 5
####     Codota      5
####     MySQL Explain 5
####     Request-mapper  5
####     Jenkins Control 5    新版Jenkins配置Token使用
####     Alibaba Java Coding Guidelines  4
***
####     .ignore 5
####     GitToolBox 5
####     Git Flow Integration 4
####     Git Commit Template 4
***
####     Java8 Postfix  5
####     Guava Postfix Completion   4
####     Lin Postfix    3
####     Custom Postfix Templates    3
***
####     MyBatisCodeHelperPro   5
####     MyBatis Log Plugin     5
####     idea-mybatis-generator 3 代码生成器
####     mybatis-generator-plus 3 代码生成器
####     EasyCodeMybatisCodeHelper 4 代码生成器
***
####     JavaDoc 3
####     String Manipulation    5
####     GenerateAllSetter      5
####     GsonFormat     5
####     JUnitGenerator V2.0    3
***
####     Rainbow Brackets       5
####     HighlightBracketPair   5
####     SequenceDiagram        5
####     Grep Console   4
####     Convert YAML and Properties File  4
####     IDEA Mind Map  3
####     VisualVM Launcher	5
***
####     Shifter    3
####     AceJump    3
***
####     Copy REST Url  3
####     Api Debugger   3
####     RestfulToolkit 3
***
####     Vue.js 5
####     IntelliVue     5
####     NativeScript   5
####     TypoScript - Enterprise    5
####     Live Edit  4
***
####     CMD-Support    4
####     BashSupport    3
***     
####     Cyan Light Theme 4
####     Nord   3

